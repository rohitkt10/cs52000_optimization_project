#!/usr/bin/env python

from stiefel_opt import *
import numpy as np
from scipy.linalg import orth

#define the objective function; this function is to be maximized
def obj_func(X):
    """
    param X:: type<numpy.ndarray>
        X is a point on the Stiefel manifold.
    
    Return the function and the Euclidean gradient at
    the point X.
    """
    
    #check that is symmetric
    shape = X.shape
    assert shape[0] >= shape[1], 'X not on the Stiefel manifold !'
        
    #compute function output.
    f = np.trace(np.dot(np.dot(X.T, B), X))
    f_grad = 2*np.dot(B, X)
    return -f, -f_grad


if __name__ == '__main__':
    #generate the B matrix
    n=400
    p=20
    B = np.random.randn(n, n)
    B = np.dot(B.T, B)  #to make B a symmetric P.D. definite matrix.
    #B = (B + B.T)/2.   #to make B a symmetric matrix but not positive definite
    
    #correct answer:
    true_solution = np.sum(np.linalg.eigvals(B)[:p])
    
    #generate the initial starting point 
    X_init = np.random.randn(n, p)
    X_init = orth(X_init)   #orthogonalize using Gram-Schmidt
    
    #get the optimal solution
    res = optimize_restarts(X_init, func=obj_func, maxiter = 1000, miter = 30, disp = True,
                            epsilon = 1e-8, num_restarts=1, rho1=0.1, rho2=0.9)
    
    print "Correct answer:" + str(true_solution)
    print "Solution obtained:" + str(-res['F_opt'])