#!/usr/bin/env python


__all__ = ['optimize', 'optimize_restarts']

import numpy as np
from pdb import set_trace as keyboard
from scipy.linalg import orth
np.random.seed(0)

def W(X, G):
    """
    param X:: type<numpy.ndarray>
        Input point on the Stiefel manifold.
    param G:: type<numpy.ndarray>
        Gradient of the objective function in Euclidean metric.
    """
    
    return np.dot(G, X.T) - np.dot(X, G.T)


def Y(tau, W, X):
    """
    param tau:: type<float>
        Step size.
    param W:: type<numpy.ndarray>
        Skew symmetric matrix in Cayley transform update scheme.
    param X:: type<numpy.ndarray>
        Point on the Stiefel manifold. 
    """
    assert tau >= 0, 'Step size should be non-negative.'
    I = np.eye(*W.shape)
    B = I + 0.5 * tau * W
    C = I - 0.5 * tau * W
    Q = np.linalg.solve(B, C)
    return np.dot(Q, X)


def Y_prime(tau, W, X):
    """
    param tau:: type<float>
        Step size.
    param W:: type<numpy.ndarray>
        Skew symmetric matrix in Cayley transform update scheme.
    param X:: type<numpy.ndarray>
        Point on the Stiefel manifold. 
    """
    I = np.eye(*W.shape)
    D = I + (tau/2.)*W
    C = (X +Y(tau, W, X))/2.
    Q = -np.linalg.solve(D, W)
    return np.dot(Q, C)



def curvilinear_search(X, W, func, tau_max=0.5, rho1=0.1, rho2=0.9, miter=100):
    """
    param X:: type<numpy.ndarray>
        Current point on descent curve.
    param W:: type<numpy.ndarray>
        Skew-symmetric matrix.
    param func:: type<function>
        Objective function to be minimized passed as a python function.
    param tau_max:: type<float>
        Maximum curvilinear search step size.
    param rho1:: type<float>
        Armijo-Wolfe parameter. 
    param rho2:: type<float>
        Armijo-Wolfe parameter.
    """
    #initialize the step size variable 
    tau = tau_max
    Y0 = Y(0., W, X)
    Y0_prime = Y_prime(0., W, X)
    F0, G0 = func(Y0)
    
    #Compute F_prime(Y(tau)) for tau = 0
    F0_prime = np.trace(np.dot(G0.T, Y0_prime))
    beta=0.7
    ##keyboard()
    #begin curvilinear search
    for i in xrange(miter):
        #check for Armijo-Wolfe conditions
        if i > 0:
            if (Fi <= F0 + rho1*tau*F0_prime) and (Fi_prime >= rho2*F0_prime):
                break
            else:
                tau *= beta
        
        #get Y(tau) and Y_prime(tau) at the current value of tau
        Yi = Y(tau, W, X)
        Yi_prime = Y_prime(tau, W, X)
        
        #Get F and F_prime at the current value of Y(tau) and Y_prime(tau)
        Fi, Gi = func(Yi)
        Fi_prime = np.trace(np.dot(Gi.T, Yi_prime))
    
    #return the best estimate of the next trial point
    return Yi   


def optimize(X0, func, epsilon=1e-5, rho1=0.1, rho2=0.9,
             maxiter=1000, miter = 30, tau_max=0.5, disp = False):
    """
    param X0:: type<numpy.ndarray>
         Initial guess. Must lie on the Stiefel Manifold.
    param func:: type<function>
        Objective function to be minimized passed as a python function.
    param epsilon:: <type float>
        Tolerance for optimization routine.
    param rho1:: type<float>
        Armijo-Wolfe parameter. 
    param rho2:: type<float>
        Armijo-Wolfe parameter.
    param maxiter: type<int>
        Max. number of iterations of the optimization routine.
    param miter: type<int>
        Max. number of iterations of the curvilinear search.
    param tau_max: type<float>
        Max. step size for curvilinear search.
    param disp:: type<bool>
        Whether to display progress in optimization routine.
    """
    #initial trial point, objective function and Euclidean gradient values.
    X=X0
    F, G = func(X0)
    
    if disp:
        print '{0:4s} {1:11s} {2:5s}'.format('It', 'F', '(F - F_old) / F_old')
        print '-' * 30
    
    #start iterating
    for i in xrange(maxiter):
        #save the old trial point and obj. function values.
        X_old = X
        F_old = F    
        G_old = G
        
        #compute W at current trial point  
        W_i=W(X, G)
        
        #generate new trial point using curvilinear search
        X = curvilinear_search(X, W_i, func, tau_max=tau_max, miter=miter, rho1=rho1, rho2=rho2)
        
        #update the value of the objective function and gradient
        F, G = func(X)
        
        #compute relative error
        relative_error = np.abs((F - F_old) / F_old) 
        
        if disp:
            print '{0:4s} {1:4.5f} {2:5e}'.format(str(i).zfill(4), F, relative_error)
            
        #check for convergence
        if relative_error < epsilon or i+1 > maxiter:
            print "Converged !"
            res = {'F_opt': F, 
                   'X_opt': X,
                   'iter':i+1}
            return res
    


def optimize_restarts(X0, func, epsilon=1e-5, rho1=0.9, rho2=0.1,
             maxiter=1000, miter = 30, tau_max=0.5, disp = False, num_restarts=10):
    """
    This function is used to perform the optimization routine repeatedly using
    random initial starting points
    """
    assert num_restarts > 0
    assert type(num_restarts) == int
    counter=0
    if disp:
        print "Performing random restarts of the optimization routine:"
        print "="*54
    while True:
        if counter == num_restarts:
            break
        if disp:
            print "Optimization restarts({0:2s}/{1:2s})".format(str(counter+1).zfill(2), str(num_restarts).zfill(2))
            print "="*28
        if counter > 0:
            X0 = np.random.randn(*X0.shape)
            X0 = orth(X0)
        res = optimize(X0=X0, func=func, epsilon=epsilon, rho1=rho1, rho2=rho2,
                       maxiter=maxiter, miter=miter, tau_max=tau_max, disp=disp)
        if counter == 0:
            res_opt=res
        else:
            if res['F_opt'] < res_opt['F_opt']:
                res_opt = res
        counter+=1
    return res